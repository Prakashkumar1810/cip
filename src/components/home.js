import React, { useEffect, useState } from 'react';
import {Link} from 'react-router-dom';

function Home(){
    
    const [button,setbutton] = useState(<div>You are logged out</div>);
    
    useEffect(()=>{
        const token = localStorage.getItem("jwttoken");
        if(token !== null){
            setbutton(<div className="butt" onClick={changeState}>Logout</div>);
        }
    },[])
    
    function changeState(){
        console.log("Hello");
        localStorage.removeItem("jwttoken");
        setbutton(<div>You are logged out</div>);
    }
    
    return(
        <div>
            <h1>Bi-O-Auth</h1>
            <div className="butts">
                {button}
            </div>
        </div>
    );
}

export default Home;