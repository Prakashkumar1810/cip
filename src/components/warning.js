import React, { useEffect } from 'react';
import {useLocation} from 'react-router-dom';

function Warning(){
    const {state} = useLocation();
    
    useEffect(()=>{
        console.log(state);
    },[]);
      
    function allow(){
        window.location.href = state.state.redirecturi;
    }
    
    function deny(){
        window.location.href = "http://localhost:3082/login";
    }
    
    return(
        <div className="warning">
            Thirdparty website is trying to access the following details:
            {
                state.state.scopes.map((name,index)=>(
                    <li key={index}>{name}</li>
                ))
            }
            <div className="butts">
                <div className="butt" onClick={allow}>Allow</div>
                <div className="butt" onClick={deny}>Deny</div>
            </div>
        </div>
    );
}

export default Warning;