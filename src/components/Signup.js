import React, { useState } from 'react';
import {useHistory} from 'react-router-dom';

import './Signup.css';

function useNavigate(){
    const history = useHistory();
    return history;
}

function Signup(){
    
    //localStorage.getItem("facedescriptor");
    
    const [name, setname] = useState("");
    const [email, setemail] = useState("");
    const [phone, setphone] = useState("");
    const [gender, setgender] = useState("");
    const [bloodgroup, setbloodgroup] = useState("");
    const [country, setcountry] = useState("");
    
    const history = useNavigate();
    
    async function auth(token){
        const query = JSON.parse(localStorage.getItem("qparams"));
        localStorage.removeItem("qparams");
        let data = {
          token: token,
          clientid: query.clientid,
          scopes: query.scopes,
          redirecturi: query.redirecturi
        };
        console.log(data);
        let response = await fetch("http://localhost:3081/auth",{
          method: "POST",
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          body: JSON.stringify(data)
        });
        let result = await response.json();
        console.log(result);
        if(result.token){
            //window.location.href = data.redirecturi+"?token="+result.token;
            history.push('/warning', { state: { scopes: data.scopes, redirecturi: data.redirecturi+"?token="+result.token} });
        }
    }
    
    function handleChange(event){
        if(event.target.id === "name"){
            setname(event.target.value);
        }
        if(event.target.id === "email"){
            setemail(event.target.value);
        }
        if(event.target.id === "phone"){
            setphone(event.target.value);
        }
        if(event.target.id === "gender"){
            setgender(event.target.value);
        }
        if(event.target.id === "bloodgroup"){
            setbloodgroup(event.target.value);
        }
        if(event.target.id === "country"){
            setcountry(event.target.value);
        }
    }
    
    async function signup(){
        console.log("Name: "+name);
        console.log("Email: "+email);
        let temp = localStorage.getItem("facedescriptor");
        let fidesc;
        if(temp !== null){
            fidesc = JSON.parse(temp);
            //do faceapi.LabeledFaceDescriptors.fromJSON(fidesc); in backend before comparision;
            console.log("Face descriptor: ");
            console.log(fidesc);
        }
        let data = {
            name: name,
            email: email,
            facedesc: fidesc.descriptors[0],
            phone: phone,
            gender: gender,
            blood: bloodgroup,
            country: country
            //client: "google.com",
            //redirectURI: "http://localhost:3082"
        }
        let response = await fetch("http://localhost:3081/signup/user",{
            method: "POST",
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data)
        });
        let result = await response.json();
        console.log(result);
        if(result.token){
            auth(result.token);
        }
    }
    
    return(
        <div className="form">
            <div>Name: <input id="name" value={name} onChange={handleChange} type="text"/></div>
            <div>Email: <input id="email" value={email} onChange={handleChange} type="text"/></div>
            <div>Phone: <input id="phone" value={phone} onChange={handleChange} type="text"/></div>
            <div>Gender: <input id="gender" value={gender} onChange={handleChange} type="text"/></div>
            <div>Bloodgroup: <input id="bloodgroup" value={bloodgroup} onChange={handleChange} type="text"/></div>
            <div>Country: <input id="country" value={country} onChange={handleChange} type="text"/></div>
            <div className="signup butt" onClick={signup}>Signup</div>
        </div>
    );
}

export default Signup;