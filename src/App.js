import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import VideoCom from './components/VideoCom';
import Signup from './components/Signup';
import Warning from './components/warning';
import Home from './components/home';
import './App.css';

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/" exact>
            <Home/>
          </Route>
          <Route path="/login">
            <VideoCom/>
          </Route>
          <Route path="/signup">
            <Signup/>
          </Route>
          <Route path="/warning">
            <Warning/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
